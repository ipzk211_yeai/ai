from sklearn.model_selection import train_test_split, cross_val_score
from utilities import visualize_classifier
from sklearn.svm import SVC
import numpy as np
from sklearn import metrics
from sklearn.preprocessing import StandardScaler

input_file = 'data_multivar_nb.txt'

data = np.loadtxt(input_file, delimiter=',')
X, y = data[:, :-1], data[:, -1]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=1, stratify=y)
sc = StandardScaler()
sc.fit(X_train)
X_train_std = sc.transform(X_train)
X_test_std = sc.transform(X_test)
svc = SVC()
svc.fit(X_train_std, y_train)
y_predict = svc.predict(X_test_std)
visualize_classifier(svc, X_test, y_test)

print("Accuracy score %.3f" % (metrics.accuracy_score(y_test, y_predict)))
print("Precision score %.3f" % (metrics.precision_score(y_test, y_predict, average='weighted')))
print("Recall score %.3f" % (metrics.recall_score(y_test, y_predict, average='weighted')))
print("F1 score %.3f" % (metrics.f1_score(y_test, y_predict, average='weighted')))

