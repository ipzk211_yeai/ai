import cv2
import cvzone
import numpy as np
from cvzone.ColorModule import ColorFinder

totalMoney = 0

MyColorFinder = ColorFinder(False)
hsvVals = {'hmin': 0, 'smin': 0, 'vmin': 145, 'hmax': 63, 'smax': 91, 'vmax': 255}


def empty(a):
    pass



def preProcessing(img):
    imgPre = cv2.GaussianBlur(img, (5, 5), 3)

    thresh1 = 170
    thresh2 = 190
    imgPre = cv2.Canny(imgPre, thresh1, thresh2)
    kernel = np.ones((3, 3), np.uint8)
    imgPre = cv2.dilate(imgPre, kernel, iterations=1)
    imgPre = cv2.morphologyEx(imgPre, cv2.MORPH_CLOSE, kernel)

    return imgPre


img = cv2.imread('coins_2.JPG')
imgPre = preProcessing(img)
imgContours, conFound = cvzone.findContours(img, imgPre, minArea=20)
imgcount = np.zeros((480, 640, 3), np.uint8)

if conFound:
    for count, contour in enumerate(conFound):
        peri = cv2.arcLength(contour['cnt'], True)
        approx = cv2.approxPolyDP(contour['cnt'], 0.02 * peri, True)

        if len(approx) > 5:
            area = contour['area']
            x, y, w, h = contour['bbox']
            imgCrop = img[y:y + h, x:x + w]
            imgColor, mask = MyColorFinder.update(imgCrop, hsvVals)
            whitePixelCount = cv2.countNonZero(mask)

            if area < 2050:
                totalMoney += 20
            elif 2050 < area < 2500:
                totalMoney += 1
            else:
                totalMoney += 5
    cvzone.putTextRect(imgcount, f'Rs.{totalMoney}', (100, 200), scale=10, offset=30, thickness=7)

    imgStacked = cvzone.stackImages([img, imgPre, imgContours, imgcount], 2, 1)
    cvzone.putTextRect(imgStacked, f'Rs.{totalMoney}', (50, 50))
    cv2.imshow("Image", imgStacked)
    cv2.waitKey()
