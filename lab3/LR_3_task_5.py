import numpy as np
from sklearn import linear_model
from sklearn.preprocessing import PolynomialFeatures
import matplotlib.pyplot as plt

m = 100
X = 6 * np.random.rand(m, 1) - 3

Xlist = [x[0] for x in X]
Xlist.sort()
X = np.array([np.array([x]) for x in Xlist])

y = 0.4 * X ** 2 + X + 4 + np.random.randn(m, 1)

poly_features = PolynomialFeatures(degree=2, include_bias=False)
X_poly = poly_features.fit_transform(X)

print("X[0] = ", X[0])
print("X_poly[0] = ", X_poly[0])


lin_reg = linear_model.LinearRegression()
lin_reg.fit(X_poly, y)

print("lin_reg.intercept_", lin_reg.intercept_)
print("lin_reg.coef_", lin_reg.coef_)

y_test_pred_lin = lin_reg.predict(X_poly)

plt.scatter(X, y, color='green')
plt.plot(X, y_test_pred_lin, color='black', linewidth=2)
plt.xticks(())
plt.yticks(())
plt.show()

