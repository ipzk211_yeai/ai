import sklearn
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans
from sklearn.datasets import load_iris
from sklearn.svm import SVC
from sklearn.metrics import pairwise_distances_argmin
import numpy as np

iris = load_iris()
X = iris['data']
y = iris['target']
#Ініціалізація кластеризування
sklearn.cluster.KMeans(n_clusters=8, init='k-means + +', n_init=10, max_iter=
300, tol=0.0001, verbose=0, random_state=
                       None, copy_x=True, algorithm='auto')
#Генерація 3 центрів кластерів
kmeans = KMeans(n_clusters=3)
#Обрахування кластеризації
kmeans.fit(X)
#Передбачення найближчого центру кластеру для даних
y_kmeans = kmeans.predict(X)
#Додавання центрів кластерів до графіку
plt.scatter(X[:, 0], X[:, 1], c=y_kmeans, s=50, cmap='viridis')
centers = kmeans.cluster_centers_
plt.scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5)

#Визначення функції для пошуку кластерів
def find_clusters(X, n_clusters, rseed=2):
    #Вибір випадкового центру кластеру
    rng = np.random.RandomState(rseed)
    i = rng.permutation(X.shape[0])[:n_clusters]
    centers = X[i]
    while True:
        #Пошук відстаней між даними та центрами кластерів
        labels = pairwise_distances_argmin(X, centers)
        #Збереження нових центрів
        new_centers = np.array([X[labels == i].mean(0)
                                for i in range(n_clusters)])
        #Перевірка, якщо дані перестають змінюватись, то перестаємо шукати кластери
        if np.all(centers == new_centers):
            break
        centers = new_centers
    return centers, labels


centers, labels = find_clusters(X, 3)
plt.scatter(X[:, 0], X[:, 1], c=labels,
            s=50, cmap='viridis')
#Отримання результатів з функції обрахування центрів та додавання даних до графіку
centers, labels = find_clusters(X, 3, rseed=0)
plt.scatter(X[:, 0], X[:, 1], c=labels,
            s=50, cmap='viridis')
#Додавання передбачених даних до графіку
labels = KMeans(3, random_state=0).fit_predict(X)
plt.scatter(X[:, 0], X[:, 1], c=labels,
            s=50, cmap='viridis')
plt.show()
